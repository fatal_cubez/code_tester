import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class Tester {

	public static String main(String[] args) {
		StringBuilder sb = new StringBuilder();
		try {
			for (String testClass : args) {
				Result result = JUnitCore.runClasses(Class.forName(testClass));
				for (Failure failure : result.getFailures()) {
					sb.append(failure);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
}
