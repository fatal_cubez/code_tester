package server;

import java.net.Socket;

import packet.Packet;

public abstract class ClientHandler implements Runnable{

	private Packet packet;
	protected Thread thread;
	private Socket socket;
	
	public ClientHandler(Packet packet, Socket socket) {
		this.packet = packet;
		this.socket = socket;
		thread = new Thread(this);
	}
	
	public abstract String getMessage();
	
	public final void handle() {
		thread.start();
	}
	
	public final boolean isFinished(){
		return !thread.isAlive();
	}
	
	protected Packet getPacket() {
		return packet;
	}
	
	public Socket getSocket() {
		return socket;
	}
	
}
