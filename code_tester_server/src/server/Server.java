package server;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import packet.FileSubmissionPacket;
import packet.Packet;

public class Server extends Thread {

	private ServerSocket server;
	private int port;
	private List<ClientHandler> handlers;
	private Thread listenThread;
	private Thread handlerThread;

	// LIFECYCLE
	// ---------
	// 1. Start up server and wait for connections
	// 2. Upon connection, get files and save them to temporary directory
	// 3. Figure out what tests belong to submitted lab
	// 4. Run JUnit tests with student code
	// 5. Send necessary information back to student
	// 6. Close connection

	public Server(int port) {
		this.port = port;
		setup();
		start();
	}

	private void setup() {
		try {
			server = new ServerSocket(port);
			System.out.println("Server started on port: " + port);
		} catch (IOException e) {
			System.out.println("Failed to start server on port: " + port);
			System.exit(0);
		}
		handlers = new ArrayList<>();

		listenThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						Socket client = server.accept();
						ObjectInputStream input = new ObjectInputStream(client.getInputStream());
						Packet packet = (Packet) input.readObject();

						ClientHandler handler = null;
						switch (packet.getType()) {
						case FILE_SUBMISSION:
							FileSubmissionPacket filePacket = (FileSubmissionPacket) packet;
							handler = new StudentHandler(packet, client, filePacket.getStudentID());
							break;
						case LAB_SUBMISSION:
							handler = new TeacherHandler(packet, client);
							break;
						default:
							break;
						}

						if (handler != null) {
							handler.handle();
							addHandler(handler);
						}

					} catch (IOException e) {
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		});

		handlerThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						if(handlers.size() == 0) Thread.sleep(20);
						for (int i = 0; i < handlers.size(); i++) {
							ClientHandler handler = handlers.get(i);
							if (handler.isFinished()) {
								String response = handler.getMessage();
								System.out.println("Writing to the client...");
								OutputStream out = handler.getSocket().getOutputStream();
								Writer writer = new BufferedWriter(new PrintWriter(out));
								writer.write(response);
								writer.flush();
								writer.close();
								out.close();
								removeHandler(i);
								i--;
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	public void startServer() {
		listenThread.start();
		handlerThread.start();
	}

	private synchronized void removeHandler(int index) {
		handlers.remove(index);
	}

	private synchronized void addHandler(ClientHandler handler) {
		handlers.add(handler);
	}

}
