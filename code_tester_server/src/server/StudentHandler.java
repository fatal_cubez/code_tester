package server;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.Method;
import java.net.Socket;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import data.FileGroup;
import data.FileInfo;
import data.Lab;
import data.TestClass;
import data.TestFile;
import packet.FileSubmissionPacket;
import packet.Packet;

public class StudentHandler extends ClientHandler {

	private final Pattern packagePattern = Pattern.compile("package [a-zA-Z0-9]*;");
	private int studentID;
	private String message;
	
	public StudentHandler(Packet packet, Socket socket, int studentID) {
		super(packet, socket);
		this.studentID = studentID;
	}
	
	@Override
	public void run() {
		FileSubmissionPacket filePacket = (FileSubmissionPacket) getPacket();
		handleFileSubmission(filePacket);
		runTest(filePacket.getLabName());
	}
	
	@Override
	public String getMessage() {
		return message;
	}
	
	private void handleFileSubmission(FileSubmissionPacket packet) {
		FileGroup group = packet.getFileGroup();
		String labName = packet.getLabName();
		Lab lab = getLab(labName);
		if(lab == null) {
			// Send response back to client
			System.out.println(labName + " is not a valid lab.");
			return;
		}
		
		if(!lab.hasRequiredFiles(group)) {
			// Required files weren't submitted
			System.out.println("Required files weren't submitted");
			return;
		}
		
		// For each test file, find the related submitted file
		// and adjust import statements as needed
		
		Set<TestClass> requiredClasses = lab.getRequiredTestClasses();
		Map<String, String> packageMap = new HashMap<String, String>(); // key: class name, value: package name
		
		for(TestClass testClass : requiredClasses) {
			FileInfo file = group.getFile(testClass.getName() + ".java");
			String fileData = file.getFileData();
			
			// Get package info
			Matcher matcher = packagePattern.matcher(fileData);
			String newPackage = "";
			if(matcher.find()) {
				newPackage = matcher.group();
				newPackage = newPackage.substring(8, newPackage.length() - 1);
			}
			packageMap.put(testClass.getName(), newPackage);
		}
		
		Path tmpDir = getTmpDir();
		try {
			Files.createDirectory(tmpDir);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	
		for(TestFile testFile : lab.getTestFiles()) {
			try(BufferedWriter writer = Files.newBufferedWriter(tmpDir.resolve(testFile.getFileName()))) {
				String imports = "";
				for(TestClass testClass : testFile.getRequiredClasses()){
					if(!packageMap.get(testClass.getName()).isEmpty()) {
						imports += "import " + packageMap.get(testClass.getName()) + "." + testClass.getName() + ";\n";
					}
				}
				String data = testFile.getFileData();
				Matcher matcher = packagePattern.matcher(testFile.getFileData());
				if(matcher.find()) {
					int split = matcher.end();
					data = data.substring(split);
				}
				writer.write(imports);
				writer.write(data);
				writer.close();
			}catch (IOException e){
				e.printStackTrace();
			}
			
		}
		// Save Code files
		for(FileInfo file : group.getFiles()) {
			try(BufferedWriter writer = Files.newBufferedWriter(tmpDir.resolve(file.getFileName()), StandardOpenOption.CREATE)){
				writer.write(file.getFileData());
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}
	
	private void runTest(String labName) {
		// Make directory
		Path compiled = getCompiledDir();
		try {
			Files.createDirectory(compiled);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		Lab lab = getLab(labName);
		List<File> allFiles = getFiles(lab);
		allFiles.add(Paths.get("templates", "Tester.java").toFile());

		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
		StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, Locale.getDefault(), null);
		List<String> options = new ArrayList<String>();
		options.add("-d");
		options.add(compiled.toAbsolutePath().toString());
		Iterable<? extends JavaFileObject> objects = fileManager.getJavaFileObjectsFromFiles(allFiles);
		CompilationTask task = compiler.getTask(null, fileManager, null, options, null, objects);
		task.call();
		
		try {
			URLClassLoader loader = new URLClassLoader(new URL[] { compiled.toUri().toURL() } );
			Class<?> cls = Class.forName("Tester", true, loader);
			Method mainMethod = cls.getMethod("main", String[].class);
			String[] testFiles = new String[lab.getTestFiles().size()];
			for(int i = 0; i < testFiles.length; i++) {
				testFiles[i] = lab.getTestFiles().get(i).getFileName().replaceAll("\\.java", "");
			}
			message = (String) mainMethod.invoke(null, (Object) testFiles);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			deleteRecursive(compiled);
			deleteRecursive(getTmpDir());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private List<File> getFiles(Lab lab) {
		List<File> files = new ArrayList<File>();
		try {
			Files.walkFileTree(getTmpDir(), new FileVisitor<Path>() {
				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					return FileVisitResult.CONTINUE;				}

				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					files.add(file.toFile());
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
					return FileVisitResult.TERMINATE;				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					return FileVisitResult.CONTINUE;
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return files;
	}

	public void deleteRecursive(Path path) throws IOException{
		if (!Files.exists(path)) throw new FileNotFoundException();
        if (Files.isDirectory(path)){
        	File dir = path.toFile();
        	for (File f : dir.listFiles()){
                deleteRecursive(Paths.get(f.toURI()));
            }
        }
        Files.delete(path);
	}
	
	private Lab getLab(String labName){
		Path path = Paths.get("labs", labName + ".lab");
		if(!Files.exists(path)) return null;
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path.toFile()));
			Lab lab = (Lab) ois.readObject();
			ois.close();
			return lab;
			
		}catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private Path getTmpDir() {
		return Paths.get("tmp" + studentID);
	}
	
	private Path getCompiledDir() {
		return Paths.get("compiled" + studentID);
	}

}
