package server;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import data.Lab;
import packet.LabSubmissionPacket;
import packet.Packet;

public class TeacherHandler extends ClientHandler{

	private String message;
	
	public TeacherHandler(Packet packet, Socket socket) {
		super(packet, socket);
	}

	@Override
	public void run() {
		LabSubmissionPacket labPacket = (LabSubmissionPacket) getPacket();
		handleLab(labPacket.getLab());
	}

	private void handleLab(Lab lab){
		try {
			// Save the lab
			Path path = Paths.get("labs", lab.getName() + ".lab");
			if(Files.exists(path)) {
				Files.delete(path);
			}
			Files.createFile(path);
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path.toFile()));
			oos.writeObject(lab);
			oos.flush();
			oos.close();
			message = "Successfully submitted " + lab.getName() + " lab.";
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public String getMessage() {
		return message;
	}

}
