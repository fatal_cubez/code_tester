package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import data.FileGroup;
import data.FileInfo;
import packet.FileSubmissionPacket;
import packet.Packet;

public class Client extends Thread{

	private int port;
	private String lab;
	private int studentID;
	
	// LIFECYCLE
	// ---------
	// 1. Connect to server
	// 2. Locate and package up source files into packet
	// 3. Send files to server and wait for response
	// 4. Print server feedback to console
	
	public Client(int port, String lab, int studentID){
		this.port = port;
		this.lab = lab;
		this.studentID = studentID;
	}
	
	public void submit() {
		start();
	}
	
	@Override
	public void run() {
		try{
			Path rootFolder = Paths.get("src", "samplecode");
			
			Socket echoSocket = new Socket("localhost", port);
			ObjectOutputStream out = new ObjectOutputStream(echoSocket.getOutputStream());
		
			System.out.println("Connected to server on port: " + port);
			
			Packet packet = createFileSubmission(rootFolder);
			out.writeObject(packet);
			out.flush();
			
			System.out.println("Waiting for response...");
			BufferedReader reader = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
			String line = null;
			
			while((line = reader.readLine()) != null) {
				System.out.println(line);
			}
			
			System.out.println("Closing client connection.");
			echoSocket.close();
			out.close();
			
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private FileSubmissionPacket createFileSubmission(Path rootPath){
		FileGroup fileGroup = new FileGroup();
		List<FileInfo> files = getFilesRecursive(rootPath);
		fileGroup.addAll(files);
		return new FileSubmissionPacket(fileGroup, lab, studentID);
	}
	
	// HANDLE What problems can arise from traversing file tree?
	private List<FileInfo> getFilesRecursive(Path rootPath) {
		List<FileInfo> files = new ArrayList<FileInfo>();
		try {
			Files.walkFileTree(rootPath, new FileVisitor<Path>() {
				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					String fileName = file.toFile().getName();
					String fileData = String.join("\n", Files.readAllLines(file));
					files.add(new FileInfo(fileName, fileData));
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
					return FileVisitResult.TERMINATE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					return FileVisitResult.CONTINUE;
				}
				
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return files;
	}
	
	
}
