package main;

import client.Client;

public class StudentMain {

	public static void main(String[] args) {
		String lab = args[0];
		int id = -1;
		try {
			id = Integer.parseInt(args[1]);
		} catch(NumberFormatException e) {
			e.printStackTrace();
		}
		Client client = new Client(4444, lab, id);
		client.submit();
	}

}
