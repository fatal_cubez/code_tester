package samplecode;
/**
 * Represents an immutable fraction that is simplified.
 * 
 * @author Scott
 *
 */
public class Fraction implements Comparable<Fraction>{

	private int numerator;
	private int denominator;
	
	/**
	 * Initializes a default fraction of 1/1.
	 */
	public Fraction(){
		this(1,1);
	}
	
	/**
	 * Creates a fraction 1/denominator.
	 * 
	 * @param denominator
	 */
	public Fraction(int denominator){
		this(1, denominator);
	}
	
	/**
	 * Creates a fraction with the numerator and denominator of the inputs.
	 * 
	 * @param numerator
	 * @param denominator
	 */
	public Fraction(int numerator, int denominator){
		if(denominator < 0){
			numerator = -numerator;
			denominator = -denominator;
		}
		this.numerator = numerator;
		this.denominator = denominator;
		simplify();
	}
	
	/**
	 * Creates a fraction from a decimal form.
	 * <br><br>
	 * Note: Limited to 8 decimal places.
	 * 
	 * @param decimalForm
	 */
	public Fraction(float decimalForm){
		String string = String.format("%.8f", decimalForm);
		float reduced = Float.parseFloat(string);
		while(string.charAt(string.length() - 1) == '0') string = string.substring(0, string.length() - 1);
		denominator = (int)Math.pow(10, (string.length() - 2));
		numerator = (int)(reduced * denominator);
		simplify();
	}
	
	/**
	 * Adds fraction other and returns a new fraction based on the result.
	 * 
	 * @param other
	 * @return
	 */
	public Fraction add(Fraction other){
		return add(this, other);
	}

	/**
	 * Subtracts fraction other and returns a new fraction based on the result.
	 * 
	 * @param other
	 * @return
	 */
	public Fraction subtract(Fraction other){
		return subtract(this, other);
	}

	/**
	 * Multiplies fraction other and returns a new fraction based on the result.
	 * 
	 * @param other
	 * @return
	 */
	public Fraction multiply(Fraction other){
		return multiply(this, other);
	}
	
	/**
	 * Divides fraction other and returns a new fraction based on the result.
	 * 
	 * @param other
	 * @return
	 */
	public Fraction divide(Fraction other){
		return divide(this, other);
	}
	
	/**
	 * Returns the reciprocal of this fraction.
	 * 
	 * @return
	 */
	public Fraction reciprocal(){
		return reciprocal(this);
	}
	
	/**
	 * Returns the negated fraction.
	 * 
	 * @return
	 */
	public Fraction negate(){
		return negate(this);
	}
	
	/**
	 * Returns true if this fraction is negative.
	 * 
	 * @return
	 */
	public boolean isNegative(){
		return isNegative(this);
	}
	
	/**
	 * Returns true if this fraction is greater than the inputed fraction.
	 * 
	 * @param f1
	 * @return
	 */
	public boolean isGreaterThan(Fraction f1){
		return toDecimal() > f1.toDecimal();
	}
	
	/**
	 * Adds both fractions and returns the result.
	 * 
	 * @param f1
	 * @param f2
	 * @return
	 */
	public static Fraction add(Fraction f1, Fraction f2){
		int lcm = lcm(f1.denominator, f2.denominator);
		return new Fraction(f1.numerator * (lcm / f1.denominator) + f2.numerator * (lcm / f2.denominator), lcm);
	}
	
	/**
	 * Subtracts both fractions and returns the result.
	 * 
	 * @param f1
	 * @param f2
	 * @return
	 */
	public static Fraction subtract(Fraction f1, Fraction f2){
		int lcm = lcm(f1.denominator, f2.denominator);
		return new Fraction(f1.numerator * (lcm / f1.denominator) - f2.numerator * (lcm / f2.denominator), lcm);
	}
	
	/**
	 * Multiplies both fractions and returns the result.
	 * 
	 * @param f1
	 * @param f2
	 * @return
	 */
	public static Fraction multiply(Fraction f1, Fraction f2){
		return new Fraction(f1.numerator * f2.numerator, f1.denominator * f2.denominator);
	}
	
	/**
	 * Divides both fractions and returns the result.
	 * 
	 * @param f1
	 * @param f2
	 * @return
	 */
	public static Fraction divide(Fraction f1, Fraction f2){
		return multiply(f1, reciprocal(f2));
	}
	
	/**
	 * Returns the reciprocal of the inputed fraction.
	 * 
	 * @param f1
	 * @return
	 */
	public static Fraction reciprocal(Fraction f1){
		return new Fraction(f1.denominator, f1.numerator);
	}
	
	/**
	 * Returns the negation of the inputed fraction.
	 * 
	 * @param f1
	 * @return
	 */
	public static Fraction negate(Fraction f1){
		return multiply(f1, new Fraction(-1));
	}
	
	/**
	 * Returns true if the inputed fraction is negative.
	 * 
	 * @param f1
	 * @return
	 */
	public static boolean isNegative(Fraction f1){
		return f1.numerator < 0;
	}
	
	/**
	 * Returns true if the numerical value of each fraction is equal.
	 * 
	 * @param f1
	 * @param f2
	 * @return
	 */
	public static boolean equals(Fraction f1, Fraction f2){
		return f1.equals(f2);
	}
	
	/**
	 * Returns the decimal value of the fraction.
	 * 
	 * @return
	 */
	public float toDecimal(){
		return (float)numerator / (float)denominator;
	}
	
	/**
	 * Simplifies the fraction.
	 */
	private void simplify(){
		int gcf = gcf(numerator, denominator);
		numerator /= gcf;
		denominator /= gcf;
	}
	
	/**
	 * Returns the greatest common factor of x and y.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	private static int gcf(int x, int y){
		int gcf = 1;
		for(int i = 1; i <= Math.max(x, y); i++){
			if(x % i == 0 && y % i == 0) gcf = i;
		}
		return gcf;
	}
	
	/**
	 * Returns the least common multiple of x and y.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	private static int lcm(int x, int y){
		return (x * y) / gcf(x, y);
	}
	
	@Override
	public String toString() {
		return denominator != 1 ? numerator + "/" + denominator : "" + numerator;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + denominator;
		result = prime * result + numerator;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Fraction other = (Fraction) obj;
		if (denominator != other.denominator) return false;
		if (numerator != other.numerator) return false;
		return true;
	}

	/**
	 * Returns 1 if the inputed fraction is less than this fraction,
	 * -1 if the inputed fraction is greater than this fraction,
	 * and 0 if they're equal.
	 */
	@Override
	public int compareTo(Fraction o) {
		if(o.equals(this)) return 0;
		return subtract(o).isNegative() ? -1 : 1;
	}
	
	/**
	 * Returns the numerator.
	 * @return
	 */
	public int getNumerator(){
		return numerator;
	}
	
	/**
	 * Returns the denominator.
	 * @return
	 */
	public int getDenominator(){
		return denominator;
	}
	
}
