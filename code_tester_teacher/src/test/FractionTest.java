package test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import samplecode.Fraction;

public class FractionTest {

	private Fraction f1;
	private Fraction f2;
	
	@Before
	public void initFractions(){
		f1 = new Fraction(1, 2);
		f2 = new Fraction(-1, 3);
	}
	
	@Test
	public void addTest(){
		Fraction result = f1.add(f2);
		Fraction expected = new Fraction(2,6);
		assertThat(expected, is(result));
	}
	
	@Test
	public void subtractTest(){
		Fraction result = f1.subtract(f2);
		Fraction expected = new Fraction(5,6);
		assertEquals(expected, result);
	}
	
}
