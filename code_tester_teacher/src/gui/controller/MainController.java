package gui.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;

public class MainController {

	@FXML private AnchorPane anchorMain;
	@FXML private AnchorPane anchorTab;
	@FXML private TabPane tabPane;

	@FXML
	private void initialize() {
	}
	
	@FXML
	private void menuItemNewOnClick(ActionEvent event) {
		Tab tab = new Tab();
		tab.setText("Untitled");
		tabPane.getTabs().add(tab);
	}

	
}
