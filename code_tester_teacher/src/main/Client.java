package main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import data.Lab;
import data.TestClass;
import data.TestFile;
import packet.LabSubmissionPacket;

public class Client extends Thread{

	private int port;
	
	public Client(int port){
		this.port = port;
		start();
	}
	
	@Override
	public void run() {
		try {
			// Setup Lab
			Lab lab = new Lab("Fraction Lab", "fraction");
			Path testPath = Paths.get("src", "test", "FractionTest.java");
			TestFile fractionTest = new TestFile(testPath.toFile().getName(), String.join("\n", Files.readAllLines(testPath)));
			fractionTest.addRequiredClass(new TestClass("Fraction"));
			lab.addTestFile(fractionTest);
			
			// Send Packet
			Socket socket = new Socket("localhost", port);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			LabSubmissionPacket packet = new LabSubmissionPacket(lab);
			oos.writeObject(packet);
			oos.flush();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String line = null;
			while((line = reader.readLine()) != null) {
				System.out.println(line);
			}
			
			oos.close();
			reader.close();
			socket.close();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
