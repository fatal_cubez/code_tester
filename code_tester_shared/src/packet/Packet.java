package packet;

import java.io.Serializable;

public abstract class Packet implements Serializable{

	private static final long serialVersionUID = 5813378145132361674L;
	private PacketType type;
	
	public Packet(PacketType type){
		this.type = type;
	}
	
	public PacketType getType() {
		return type;
	}
	
}
