package packet;

import data.Lab;

public class LabSubmissionPacket extends Packet{

	private static final long serialVersionUID = -5918735328350093125L;
	private Lab lab;
	
	public LabSubmissionPacket(Lab lab){
		super(PacketType.LAB_SUBMISSION);
		this.lab = lab;
	}
	
	public Lab getLab() {
		return lab;
	}
	
	@Override
	public String toString() {
		return "Lab Submission";
	}
	
}
