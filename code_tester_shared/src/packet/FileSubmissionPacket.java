package packet;

import data.FileGroup;

public class FileSubmissionPacket extends Packet{

	private static final long serialVersionUID = 8945963537769237898L;
	
	private FileGroup fileGroup;
	private String labName;
	private int studentID;
	
	public FileSubmissionPacket(FileGroup fileGroup, String labName, int studentID){
		super(PacketType.FILE_SUBMISSION);
		this.fileGroup = fileGroup;
		this.labName = labName;
		this.studentID = studentID;
	}
	
	public FileGroup getFileGroup() {
		return fileGroup;
	}
	
	public String getLabName() {
		return labName;
	}
	
	public int getStudentID() {
		return studentID;
	}
	
	@Override
	public String toString() {
		return "File Submission - " + fileGroup.toString();
	}
}
