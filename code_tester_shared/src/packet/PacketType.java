package packet;

import java.io.Serializable;

public enum PacketType implements Serializable{

	FILE_SUBMISSION,
	LAB_SUBMISSION
	
}
