package data;

import java.util.ArrayList;
import java.util.List;

public class TestFile extends FileInfo {

	private static final long serialVersionUID = 7590370432277819985L;
	private List<TestClass> requiredClasses;
	
	public TestFile(String fileName, String fileData) {
		super(fileName, fileData);
		requiredClasses = new ArrayList<TestClass>();
	}
	
	public void addRequiredClass(TestClass testClass) {
		requiredClasses.add(testClass);
	}
	
	public List<TestClass> getRequiredClasses(){
		return requiredClasses;
	}
	
	@Override
	public String toString() {
		return "Test File: " + getFileName();
	}
	
	
}
