package data;

import java.io.Serializable;

public class FileInfo implements Serializable{

	private static final long serialVersionUID = 296363989588723784L;

	private String fileName;
	private String fileData;
	
	public FileInfo(String fileName, String fileData) {
		this.fileName = fileName;
		this.fileData = fileData;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public String getFileData() {
		return fileData;
	}
	
	@Override
	public String toString() {
		return fileName;
	}
	
}
