package data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Lab implements Serializable{

	private static final long serialVersionUID = -598799871283801370L;
	private String displayName;
	private String name;
	private int id;
	private List<TestFile> testFiles;
	
	public Lab(String displayName, String name){
		this.displayName = displayName;
		this.name = name;
		id = name.hashCode();
		testFiles = new ArrayList<TestFile>();
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getName() {
		return name;
	}
	
	public void addTestFile(TestFile testFile){
		testFiles.add(testFile);
	}
	
	public List<TestFile> getTestFiles() {
		return testFiles;
	}
	
	public Set<TestClass> getRequiredTestClasses() {
		Set<TestClass> requiredClasses = new HashSet<TestClass>();
		for(TestFile file : testFiles){
			requiredClasses.addAll(file.getRequiredClasses());
		}
		return requiredClasses;
	}
	
	public boolean hasRequiredFiles(FileGroup group){
		for(TestClass testClass : getRequiredTestClasses()){
			boolean found = false;
			for(FileInfo file : group.getFiles()) {
				if((testClass.getName() + ".java").equals(file.getFileName())) {
					found = true;
					break;
				}
			}
			if(!found) return false;
		}
		return true;
	}
	
	public int getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return displayName + "[" + id + "]: " + testFiles.toString();
	}
	
}
