package data;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class TestClass implements Serializable{

	private static final long serialVersionUID = 1154898824789129218L;
	private String name;
	private List<Method> requiredMethods;
	
	public TestClass(String name) {
		this.name = name;
		requiredMethods = new ArrayList<Method>();
	}
	
	public void addMethod(Method method){
		requiredMethods.add(method);
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return name + " -> " + requiredMethods;
	}
	
}
