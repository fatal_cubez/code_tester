package data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FileGroup implements Serializable{

	private static final long serialVersionUID = 5742119390869939201L;

	private List<FileInfo> files;
	
	public FileGroup() {
		files = new ArrayList<FileInfo>();
	}
	
	public List<FileInfo> getFiles() {
		return files;
	}
	
	public void addFile(String fileName, String fileData){
		addFile(new FileInfo(fileName, fileData));
	}
	
	public void addFile(FileInfo info) {
		files.add(info);
	}
	
	public void addAll(Collection<? extends FileInfo> infos){
		files.addAll(infos);
	}
	
	public FileInfo getFile(int index) {
		return files.get(index);
	}
	
	public FileInfo getFile(String name){
		for(FileInfo file : files){
			if(file.getFileName().equals(name)) return file;
		}
		return null;
	}
	
	@Override
	public String toString() {
		return files.toString();
	}
}
